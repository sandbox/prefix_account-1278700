(function (jQuery) {

    Drupal.behaviors.pmailer_contact_sync = {
        attach: function(context) {
            jQuery('#wind').click(function(){              
                var email_array = new Array();
                jQuery.getJSON(Drupal.settings.basePath+'doneit2', function(data){
                    var items = [];

                    var users_counter = 0;
                    jQuery.each(data, function(key, val) {
                        items.push('<li id="' + key + '">Total Users :' + val + '</li>');               
                        email_array.push(val);
                        users_counter = val;
                    });
                   
                    
                        items.push('<li id="info">System is processing your user list now. Please Wait... </li>');       
                        runRecursive(0,30,users_counter);
                   
                        jQuery('<ul/>', {
                            'class': 'my-new-list',
                            html: items.join('')
                        }).appendTo('#wind');
                    
                        jQuery('<div id="loader"><img src="'+Drupal.settings.basePath+'/sites/all/modules/pmailer_contact_sync/images/ajax-loader.gif" /></div>').appendTo('#wind');                    
                    
                    jQuery('#wind').unbind('click');
                   
                });
     
            });
        }
    }
}(jQuery));


function runRecursive(start ,showCounter, total){     
    var info_processed = "";  
    // while all database have not been continue work
    if (parseInt(start) < parseInt(total)){       
        
        
        jQuery.ajax({
            url: Drupal.settings.basePath+'subscribe/'+start+'/'+showCounter,
            success: function(subcribeData){         
                
                if(parseInt(start)+30 < total){
                    info_processed = " Processing items " + (parseInt(start) + 1) + " - " + ( parseInt(start) + parseInt(showCounter)) + " of " + total + " items. <br/>";
                    jQuery('<p>'+info_processed+'</p>').appendTo('#processList');
                } else{
                    info_processed = " Processing items " + (parseInt(start) + 1) + " - " + total + " of " + total + " items. <br/>";
                    jQuery('<p>'+info_processed+'</p>').appendTo('#processList');
                }
        
                            
                start = start + 30;
                runRecursive(start,showCounter,total);               
                
                jQuery.each(subcribeData, function(key, val) {
                                            
                    jQuery('#processList').html(jQuery('#processList').html() + val +"<br/>");
                });
                
            }               
        });
    } else{
        jQuery('#loader').hide();
    }
}






