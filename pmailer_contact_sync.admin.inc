<?php

/*
 * @file
 * pMailer Contacts Add Module Admin Settings
 *
 * implementation of hook_admin_settings
 * @return <type>
 */

function pmailer_contact_sync_admin_settings(){

  drupal_add_js(drupal_get_path('module', 'pmailer_contact_sync') . '/pmailer_contact_sync.js');
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'pmailer_contact_sync') . '/pmailer_api.php';

  $form['pmailer_contact_sync_account_info'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => 'Insert pMailer API Information',
  );

  $form['pmailer_contact_sync_account_info']['pmailer_contact_sync_server'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer server'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_contact_sync_server', ''),
    '#description' => t('Specify pmailer server. '),
  );

  $form['pmailer_contact_sync_account_info']['pmailer_contact_sync_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_contact_sync_api_key', ''),
    '#description' => t('The API key for your pmailer account. '),
  );

  $api_key = variable_get('pmailer_contact_sync_api_key', FALSE);
  $server = variable_get('pmailer_contact_sync_server', FALSE);

  //$default = !empty($form_state['values']['howmany']) ? $form_state['values']['howmany'] : 1;

  $q = "";
  if ($api_key) {
    $q = new PMailerSubscriptionApiV1_0($server, $api_key);
  }
  else {
    form_set_error('', 'Please check your pMailer server and pMailer API-key settings ');
    return system_settings_form($form);
  }

  try{
    $getlists = $q->getLists();
  } catch (PMailerSubscriptionException $e){
    form_set_error('', 'Error: ' . $e->getMessage() . ' ( Please check your pMailer server and pMailer API-key settings )');
  }

  $saved_lists = variable_get('directory_lists', array());

  if (empty($getlists) === false){
    /*
     * new lists display
     */
    $form['directory_lists']['contact_list'] = array(
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#title' => t('Please select the list you want all the Drupal users to be subscribed to.'),
      '#tree' => TRUE,
    );

    foreach ($getlists['data'] as $list)
    {

      if (!isset($saved_lists[$list['list_id']]))
      {
        $saved_lists[$list['list_id']] = 0;
      }
      if ($saved_lists[$list['list_id']] && isset($saved_lists)){
        $form['directory_lists']['contact_list'][$list['list_id']] = array(
          '#type' => 'checkbox',
          '#title' => $list['list_name'],
          '#attributes' => array('checked' => 'checked'),
          '#default-value' => '0',
        );
      }
      else{
        $form['directory_lists']['contact_list'][$list['list_id']] = array(
          '#type' => 'checkbox',
          '#title' => $list['list_name'],
            //'#default-value' => '0',
        );
      }
    }
  }
  else{
    // drupal_set_message();
    form_set_error('', t('You do not have any valid pMailer mailing lists.'));
  }


  $form['options'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => 'Activate double opt-in for all subscriptions.',
  );

  $form['options']['doubleOptin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Would you like the user to confirm their subscription.'),
    //'#required' => TRUE,
    '#default_value' => variable_get('doubleOptin', ''),
    '#description' => 'Users will be send an email to confirm all subscriptions. ( double opt-in ).'
  );
  


  $form['pre']['#prefix'] = '<div style="font-size:13pt;">Please save any changes to your configuration before you run the Add Contacts process. </div></div><div id="wind" style=\' padding:10px; margin:2px; \'><span onmouseover="document.getElementById(this.id).style.background=\'#ededed\';" onmouseout="document.getElementById(this.id).style.background=\'#ffffff\';" id="addcontactsbutton" style=\'font-size:14pt; cursor:pointer; border-bottom:ridge 5px #993300; \' >Click Here to Add Contacts</span></div><div id="processList" style="color:993300; "></div>';

  /*
    $query = "SELECT * FROM  `users` ";
    $q = db_query($query);
    foreach ($q as $key => $value){
    echo $value->name;
    echo $value->mail."<br/>";
    }
   */
  $form['#validate'][] = 'pmailer_contact_sync_admin_settings_validate';

  return system_settings_form($form);
}

/**
 * validate the admin settings and serialize the saved lists into objects
 *
 * @param <type> $form
 * @param <type> $form_state
 */
function pmailer_contact_sync_admin_settings_validate($form, &$form_state){
  $server = $form['pmailer_contact_sync_account_info']['pmailer_contact_sync_server']['#value'];
  if(substr($server, 0, 7) == 'http://' || substr($server, 0, 8) == 'https://'){
    form_set_error('title', t('URL is invalid. Please remove http:// in the beginning of your URL.'));
  }
 
  //form_set_error('', 'VARIABLE DUMP.' . var_dump($form_state['values']));

  $lists = array();
  $selected_lists = array();
  $is_checked = 0;
  if (isset($form_state['values']['contact_list'])) {
    foreach ($form_state['values']['contact_list'] as $key => $selected) {
      $lists[$key] = $selected;

      if ($selected == 1) {
        $is_checked = 1;
        array_push($selected_lists, $key);
      }
    }
  }

  if ($is_checked == 0){
    form_set_error('', 'Please select atleast one pMailer subscription');
  }

  variable_set('pmailer_contact_sync_server', $form_state['values']['pmailer_contact_sync_server']);
  variable_set('pmailer_contact_sync_api_key', $form_state['values']['pmailer_contact_sync_api_key']);

  if (isset($form_state['values']['doubleOptin'])) {
    variable_set('double_optin', $form_state['values']['doubleOptin']);
  }
    
  variable_set('directory_lists', $lists);
  variable_set('selected_contacts_lists', $selected_lists);
  unset($form_state['values']['contact_list']);

}
